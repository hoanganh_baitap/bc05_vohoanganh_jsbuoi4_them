// Bài 1: Viết chương trình nhập vào ngày, tháng, năm. (Giả sử nhập hợp lệ). Tìm ngày, tháng, năm của ngày tháng năm tiếp theo và ngày tháng năm trước đó.
document.getElementById("ngayMai").onclick = function () {
  // lấy thông tin ở input
  //   console.log("yes");
  var ngay = document.getElementById("txtNgay").value * 1;
  var thang = document.getElementById("txtThang").value * 1;
  var nam = document.getElementById("txtNam").value * 1;
  //   các bước xử lý
  //   tạo biến chứa kết quả
  var ngayMai = "";
  //   xét ngày cuói trong năm
  if (thang == 12 && ngay == 31) {
    ngay = ngay - 30;
    thang = thang - 11;
    nam = nam + 1;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 2 có 29 ngày
  if (thang == 2 && ngay >= 1 && ngay <= 28) {
    ngay = ngay + 1;
    thang = thang;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (thang == 2 && ngay == 29) {
    ngay = 1;
    thang = thang + 1;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 1, 3, 5, 7, 8, 10, có 31 ngày
  if (
    (ngay == 31 && thang == 1) ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10
  ) {
    ngay = 1;
    thang = thang + 1;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (
    (ngay >= 1 && ngay <= 30 && thang == 1) ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10
  ) {
    ngay = ngay + 1;
    thang = thang;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 4, 6, 9, 11 có 30 ngày
  if ((ngay == 30 && thang == 4) || thang == 6 || thang == 9 || thang == 11) {
    ngay = 1;
    thang = thang + 1;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (
    (ngay >= 1 && ngay <= 29 && thang == 4) ||
    thang == 6 ||
    thang == 9 ||
    thang == 11
  ) {
    ngay = ngay + 1;
    thang = thang;
    nam = nam;
    ngayMai = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  // show kết quả lên giao diện
  document.getElementById("result1").innerHTML = ngayMai;
};
// xem ngày hôm qua
document.getElementById("homQua").onclick = function () {
  // lấy thông tin input
  var ngay = document.getElementById("txtNgay").value * 1;
  var thang = document.getElementById("txtThang").value * 1;
  var nam = document.getElementById("txtNam").value * 1;
  //   tạo biến chứa kết quả ngày hôm qua
  var ngayHomQua = "";
  // xét trường hợp ngày đầu năm
  if (ngay == 1 && thang == 1) {
    ngay = 31;
    thang = thang + 11;
    nam = nam - 1;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }

  //   xét tháng 2
  if (thang == 2 && ngay == 1) {
    ngay = 31;
    thang = thang - 1;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (thang == 2 && ngay >= 2 && ngay <= 29) {
    ngay = ngay - 1;
    thang = thang;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 3
  if (thang == 3 && ngay == 1) {
    ngay = 29;
    thang = thang - 1;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (thang == 3 && ngay >= 2 && ngay <= 31) {
    ngay = ngay - 1;
    thang = thang;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 4, 6, 9, 11 có 30 ngày
  if ((ngay == 1 && thang == 4) || thang == 6 || thang == 9 || thang == 11) {
    ngay = 31;
    thang = thang - 1;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (
    (ngay >= 2 && ngay <= 30 && thang == 4) ||
    thang == 6 ||
    thang == 9 ||
    thang == 11
  ) {
    ngay = ngay = 1;
    thang = thang;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  //   xét tháng 5, 7, 8, 10, 12
  if (
    (ngay == 1 && thang == 5) ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    ngay = 30;
    thang = thang - 1;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  } else if (
    (ngay >= 2 && ngay <= 31 && thang == 5) ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    ngay = ngay - 1;
    thang = thang;
    nam = nam;
    ngayHomQua = "Ngày " + ngay + " tháng " + thang + " năm " + nam;
  }
  document.getElementById("result1").innerHTML = ngayHomQua;
};

// Bài 2: Viết chương trình nhập vào tháng, năm. Hãy cho biết tháng trong năm đó có bao nhiêu ngày, bao gồm cả năm nhuận

document.getElementById("demNgay").onclick = function () {
  // lấy thông tin input
  var soThang = document.getElementById("txtSoThang").value * 1;
  var soNam = document.getElementById("txtSoNam").value * 1;
  //   tạo biến chứa đếm ngày
  var demNgay = "";
  // các bước xử lý
  // xét tháng 2
  if (soThang == 2 && soNam % 4 == 0 && soNam % 100 != 0) {
    demNgay = "Tháng 2 năm nhuần có 28 ngày";
  } else if ((soThang == 2 && soNam % 4 != 0) || soNam % 100 == 0) {
    demNgay = "Tháng 2 có 29 ngày";
  }
  //   if (soThang == 2 && soNam % 4 != 0 && soNam % 100 == 0) {
  //     demNgay = "Tháng 2 năm nhuần có 28 ngày";
  //   }
  //   xét tháng 1, 3, 5, 7, 8, 10, 12 có 31 ngày
  if (
    soThang == 1 ||
    soThang == 3 ||
    soThang == 5 ||
    soThang == 7 ||
    soThang == 8 ||
    soThang == 12
  ) {
    demNgay = "Số ngày là 31";
  }
  // xét tháng 4, 6, 9, 11 có 30 ngày
  if (soThang == 4 || soThang == 6 || soThang == 9 || soThang == 11) {
    demNgay = "Số ngày là 30";
  }
  //   Xét tháng không hợp lý
  if (soThang < 1 || soThang > 12) {
    demNgay = "sai định dạng";
  }
  //   show kết quả lên giao diện
  document.getElementById("result2").innerHTML = demNgay;
};

// Bài 3: Viết chương trìn nhập vào số nguyên có 3 chữ số. In ra cách đọc nó
document.getElementById("docSo").onclick = function () {
  // lấy thông tin input
  var soNguyen = document.getElementById("txtSoNguyen").value * 1;
  // tạo biến chưa kết quả đọc số
  var docHangTram = "";
  var docHangChuc = "";
  var docDonVi = "";
  // tách số
  // tạo biến chứa só hàng trăm, chục, đơn vị
  var hangTram = null;
  var hangChuc = null;
  var donVi = null;
  hangTram = Math.floor(soNguyen / 100);
  hangChuc = Math.floor((soNguyen / 10) % 10);
  donVi = Math.floor(soNguyen % 10);
  // các bước xử lý
  // xét số ở hàng trăm
  if (hangTram == 0) {
    docHangTram = "Không trăm";
  } else if (hangTram == 1) {
    docHangTram = "Một trăm";
  } else if (hangTram == 2) {
    docHangTram = "Hai trăm";
  } else if (hangTram == 3) {
    docHangTram = "Ba trăm";
  } else if (hangTram == 4) {
    docHangTram = "Bốn trăm";
  } else if (hangTram == 5) {
    docHangTram = "Năm trăm";
  } else if (hangTram == 6) {
    docHangTram = "Sáu trăm";
  } else if (hangTram == 7) {
    docHangTram = "Bảy trăm";
  } else if (hangTram == 8) {
    docHangTram = "Tám trăm";
  } else if (hangTram == 9) {
    docHangTram = "Chín trăm";
  }
  //   Xét số hàng chục
  if (hangChuc == 0) {
    docHangChuc = "lẻ";
  } else if (hangChuc == 1) {
    docHangChuc = "mười";
  } else if (hangChuc == 2) {
    docHangChuc = "hai mươi";
  } else if (hangChuc == 3) {
    docHangChuc = "ba mươi";
  } else if (hangChuc == 4) {
    docHangChuc = "bốn mươi";
  } else if (hangChuc == 5) {
    docHangChuc = "năm mươi";
  } else if (hangChuc == 6) {
    docHangChuc = "sáu mươi";
  } else if (hangChuc == 7) {
    docHangChuc = "bảy mươi";
  } else if (hangChuc == 8) {
    docHangChuc = "tám mươi";
  } else if (hangChuc == 9) {
    docHangChuc = "chín mươi";
  }
  //   xét số hàng đơn vị
  if (donVi == 0) {
    docDonVi = "";
  } else if (donVi == 1) {
    docDonVi = "mốt";
  } else if (donVi == 2) {
    docDonVi = "hai";
  } else if (donVi == 3) {
    docDonVi = "ba";
  } else if (donVi == 4) {
    docDonVi = "bốn";
  } else if (donVi == 5) {
    docDonVi = "lăm";
  } else if (donVi == 6) {
    docDonVi = "sáu";
  } else if (donVi == 7) {
    docDonVi = "bảy";
  } else if (donVi == 8) {
    docDonVi = "tám";
  } else if (donVi == 9) {
    docDonVi = "chín";
  }
  //   show kết quả lên giao diện
  document.getElementById(
    "result3"
  ).innerText = `${docHangTram}  ${docHangChuc} ${docDonVi}`;
};

// BÀI 4: TOÁN HỌC NÊN KHÓ QUÁ ^^ NGHỈ LÀM
